package com.metlife.starteam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExcelReportManager {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ExcelReportManager.class);
	
	public void generateExcelReport (List<File> files, List <JarDetails> jarDetails, String excelReportLocation) {
		
		FileInputStream fis = null;
		FileOutputStream fos = null;
		HSSFWorkbook workbook = null;
		
		try {
			fis = new FileInputStream(ProjectAnalyzerConst.PROJECT_INVENTORY_EXCEL_TEMPLATE);
			workbook = new HSSFWorkbook(fis);
			int excelSheetCount = workbook.getNumberOfSheets();
			
			for (int sheetIdx = 0; sheetIdx < excelSheetCount; sheetIdx++) {
				
				if (sheetIdx == 0) {
					//process project folders
				}
				if (sheetIdx == 1) {
					//process project files
					writeFileDetailsToExcel (workbook.getSheetAt(sheetIdx), files);
				}
				if (sheetIdx == 2) {
					//process project binaries
					writeJarDetailsToExcel (workbook, workbook.getSheetAt(sheetIdx), jarDetails); 
				}
			}
			fis.close();
			
			String excelReportOutputPath = excelReportLocation + ProjectAnalyzerConst.EXCEL_TEMPLATE_NAME;
			LOGGER.info ("Excel report output location:" + excelReportOutputPath);
			
			fos = new FileOutputStream(excelReportOutputPath);
			workbook.write(fos);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
				if (fos != null)
					fos.close();
				if (workbook != null)
					workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void writeJarDetailsToExcel (Workbook workbook, Sheet sheet, List <JarDetails> jarDetails) {
		
		for (int jarCount = 0; jarCount < jarDetails.size(); jarCount++) {
			
			LOGGER.info("Jar Name:" + jarDetails.get(jarCount).getJarName() + 
					" - Jar Version:" + jarDetails.get(jarCount).getJarVersion());
			
			Row row = createRow (sheet, jarCount + 1);
			Cell cell = createCell (row, 0);
			cell.setCellValue(jarDetails.get(jarCount).getJarName());
			
			cell = createCell (row, 1);
			cell.setCellValue(jarDetails.get(jarCount).getJarVersion());
			
			cell = createCell (row, 2);
			cell.setCellValue(jarDetails.get(jarCount).getJarLocation());
			
			cell = createCell (row, 3);
			if (jarDetails.get(jarCount).getDependencyMetadata().getGroupId() == null) {
				
				HSSFCellStyle style = (HSSFCellStyle) workbook.createCellStyle();
				style.setFillForegroundColor(HSSFColor.RED.index);
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(style);
			}
			cell.setCellValue(jarDetails.get(jarCount).getDependencyMetadata().getGroupId());
			
			cell = createCell (row, 4);
			if (jarDetails.get(jarCount).getDependencyMetadata().getArtifactId() == null) {
				HSSFCellStyle style = (HSSFCellStyle) workbook.createCellStyle();
				style.setFillForegroundColor(HSSFColor.RED.index);
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(style);
			}
			cell.setCellValue(jarDetails.get(jarCount).getDependencyMetadata().getArtifactId());
			
			cell = createCell (row, 5);
			if (jarDetails.get(jarCount).getDependencyMetadata().getVersion() == null) {
				
				HSSFCellStyle style = (HSSFCellStyle) workbook.createCellStyle();
				style.setFillForegroundColor(HSSFColor.RED.index);
				style.setFillPattern(CellStyle.SOLID_FOREGROUND);
				cell.setCellStyle(style);
			}
			cell.setCellValue(jarDetails.get(jarCount).getDependencyMetadata().getVersion());
		}
	}
	
	private void writeFileDetailsToExcel (Sheet sheet, List <File> fileDetails) throws IOException {
		
		for (int fileCount = 0; fileCount < fileDetails.size(); fileCount++) {
			
			LOGGER.info("File Name:" + fileDetails.get(fileCount).getName());
			
			Row row = createRow (sheet, fileCount + 1);
			Cell cell = createCell (row, 0);
			cell.setCellValue(fileDetails.get(fileCount).getName());
			
			cell = createCell (row, 1);
			cell.setCellValue(fileDetails.get(fileCount).getCanonicalPath());
		}
	}
	
	public void generateJarDependencyMappingFile (List<JarDetails> jarDetailsList, String projectRootFolder) {
		
		List <String> contentToWrite = new ArrayList<String> ();
		
		StringBuffer sb = null;
		for (JarDetails jarDetails : jarDetailsList) {
			sb = new StringBuffer("");
			contentToWrite.add (sb.append(jarDetails.getJarLocation()).append("|")
					.append(jarDetails.getDependencyMetadata().getArtifactId()).append("-")
					.append(jarDetails.getDependencyMetadata().getVersion()).toString());
		}
		
		LOGGER.info("Dependency Mapping File Location : " + projectRootFolder + "dependency.txt");
		
		Path file = Paths.get (projectRootFolder + "dependency.txt");
		try {
			Files.write (file, contentToWrite, StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param excelReportPath
	 * @return
	 */
	public List<JarDetails> readJarDetailsFromExcelInventory (String excelReportPath) {
		
		FileInputStream excelFile = null;
		HSSFWorkbook workbook = null;
		List<JarDetails> jarDetailsList = new ArrayList<JarDetails>();
		
		try {
			excelFile = new FileInputStream (excelReportPath + ProjectAnalyzerConst.EXCEL_TEMPLATE_NAME);
			workbook = new HSSFWorkbook (excelFile);
			Sheet sheet = workbook.getSheetAt(1);
			
			 for (int rowCount=0; rowCount <= sheet.getLastRowNum(); rowCount++) {
				 if (rowCount > 0) {
					 Row row = sheet.getRow(rowCount);
					 for (int columnCount=0; columnCount <= row.getLastCellNum(); columnCount++) {
						 JarDetails jarDetails = new JarDetails();
						 jarDetails.setJarName(row.getCell(0).getStringCellValue());
						 jarDetails.setJarVersion(row.getCell(1).getStringCellValue());
						 jarDetails.setJarLocation(row.getCell(2).getStringCellValue());
						 
						 MavenDependencyMetadata metadata = new MavenDependencyMetadata ();
						 metadata.setGroupId(row.getCell(3).getStringCellValue());
						 metadata.setArtifactId(row.getCell(4).getStringCellValue());
						 metadata.setVersion(row.getCell(5).getStringCellValue());
						 metadata.setExistingJarName(row.getCell(0).getStringCellValue());
						 
						 jarDetails.setDependencyMetadata(metadata);
						 jarDetailsList.add(jarDetails);
					 }
				 }
			 }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (workbook != null)
					workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return jarDetailsList;
		
	}
	
	public List<FileDetails> readFileDetailsFromExcelInventory (String excelReportPath) {
		
		FileInputStream excelFile = null;
		HSSFWorkbook workbook = null;
		List<FileDetails> fileDetailsList = new ArrayList<FileDetails>();
		
		try {
			excelFile = new FileInputStream (excelReportPath + ProjectAnalyzerConst.EXCEL_TEMPLATE_NAME);
			workbook = new HSSFWorkbook (excelFile);
			Sheet sheet = workbook.getSheetAt(0);
			
			 for (int rowCount=0; rowCount <= sheet.getLastRowNum(); rowCount++) {
				 if (rowCount > 0) {
					 Row row = sheet.getRow(rowCount);
					 for (int columnCount=0; columnCount <= row.getLastCellNum(); columnCount++) {
						FileDetails fileDetails = new FileDetails();
						fileDetails.setFileName(row.getCell(0).getStringCellValue());
						fileDetails.setFileLocations(row.getCell(1).getStringCellValue());
						fileDetails.setFileRequired(("N".equals(row.getCell(1).getStringCellValue().trim())) ? true : false);
					 }
				 }
			 }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (workbook != null)
					workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return fileDetailsList;
		
	}
	
	private Cell createCell (Row row, int columnIdx) {
		
		Cell cell = row.getCell(columnIdx);
		if (cell == null) {
			cell = row.createCell(columnIdx);
		}
		
		return cell;
	}
	
	private Row createRow (Sheet sheet, int rowIdx) {
		
		Row row = sheet.getRow(rowIdx);
		if (row == null) {
			row = sheet.createRow(rowIdx);
		}
		
		return row;
	}
}
