package com.metlife.starteam;

public class FileDetails {
	
	private String fileName;
	private String fileLocations;
	private boolean isFileRequired;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileLocations() {
		return fileLocations;
	}
	public void setFileLocations(String fileLocations) {
		this.fileLocations = fileLocations;
	}
	public boolean isFileRequired() {
		return isFileRequired;
	}
	public void setFileRequired(boolean isFileRequired) {
		this.isFileRequired = isFileRequired;
	}
	
	@Override
	public String toString() {
		return "FileDetails [fileName=" + fileName + ", isFileRequired="
				+ isFileRequired + "]";
	}	
	
}
