package com.metlife.starteam;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class ProjectAnalyzer implements IProjectAnalyzer{

	@SuppressWarnings("unchecked")
	public List<File> analyzeProject(String rootFolder) {
		File rootDir = new File (rootFolder);
		List<File> files = (List<File>) FileUtils.listFiles (rootDir, null, true);
		return ProjectAnalyzerUtil.filterFiles(files, "." + ProjectAnalyzerConst.JAR_EXTN);		
	}

	@SuppressWarnings("unchecked")
	public List<File> analyzeProjectJars(String rootFolder) {
		File rootDir = new File (rootFolder);
		List<File> files = (List<File>) FileUtils.listFiles (rootDir, new String[] {ProjectAnalyzerConst.JAR_EXTN}, true);
		return files;	
	}

}
