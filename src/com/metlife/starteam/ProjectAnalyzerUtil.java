package com.metlife.starteam;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjectAnalyzerUtil {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ProjectAnalyzerUtil.class);

	public static List<File> filterFiles(List<File> files, String fileExtn) {

		for (int fileCount = 0; fileCount < files.size(); fileCount++) {
			if (FilenameUtils.isExtension(files.get(fileCount).getName(),
					fileExtn)) {
				files.remove(fileCount);
			}
		}
		return files;
	}
	
	public static List<FileDetails> captureFileDetails(List<File> files)
			throws IOException {

		List<FileDetails> fileDetailsList = new ArrayList<FileDetails>();

		for (File file : files) {
			FileDetails fileDetails = new FileDetails();
			fileDetails.setFileName(file.getName());
			fileDetails.setFileLocations(file.getCanonicalPath());
			fileDetails.setFileRequired(true);
			fileDetailsList.add(fileDetails);
		}

		return fileDetailsList;
	}

	public static List<JarDetails> captureJarDetails (List<File> jars)
			throws IOException {

		List<JarDetails> jarDetailsList = new ArrayList<JarDetails>();

		for (File jar : jars) {
			JarDetails jarDetails = new JarDetails();
			jarDetails.setJarName(jar.getName());
			jarDetails.setJarVersion(new javaxt.io.Jar(jar).getVersion());
			jarDetails.setJarLocation(jar.getCanonicalPath());
			jarDetails.setJarChecksum(ChecksumCalculator.calculateChecksum(jar));
			//jarDetails.setJarRequired(true);
			
			MavenDependencyMetadata dependencyMetadata = getMavenMetadata (ChecksumCalculator.calculateChecksum(jar));
			dependencyMetadata.setExistingJarName(jar.getName());
			jarDetails.setDependencyMetadata(dependencyMetadata);
			jarDetailsList.add(jarDetails);
		}

		return jarDetailsList;
	}

	private static MavenDependencyMetadata getMavenMetadata (String checksum) {
		
		MavenDependencyMetadata metadata = new MavenDependencyMetadata();
		
		try {
			HttpHost proxy = new HttpHost("172.31.16.242", 8080, "http");
			DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
			CloseableHttpClient httpClient = HttpClients.custom()
			                    .setRoutePlanner(routePlanner)
			                    .build();

			HttpGet getRequest = new HttpGet("http://search.maven.org/solrsearch/select?q=1:" + checksum + "&rows=1&wt=json");
			getRequest.addHeader("accept", "application/json");

			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			
			String output;
			StringBuffer outputBuffer = new StringBuffer("");
			while ((output = br.readLine()) != null) {
				outputBuffer.append(output);
			}
			
			JSONObject jsonObj = new JSONObject(outputBuffer.toString());
			LOGGER.info ("MAVEN Compatible Dependency Found: " + jsonObj.getJSONObject ("response").getInt("numFound"));
			
			if (jsonObj.getJSONObject ("response").getInt("numFound") > 0) {
				JSONArray jsonArray = jsonObj.getJSONObject ("response").getJSONArray("docs");
				JSONObject object = (JSONObject) jsonArray.get(0);
				
				LOGGER.info(object.getString("g"));
				LOGGER.info(object.getString("a"));
				LOGGER.info(object.getString("v"));
				
				metadata.setGroupId(object.getString("g"));
				metadata.setArtifactId(object.getString("a"));
				metadata.setVersion(object.getString("v"));
			}
		} catch (ClientProtocolException e) {
			LOGGER.error(e.getMessage(), e);

		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return metadata;
	}
	
	/**
	 * 
	 * @param fileDetailsList
	 * @param jarDetailsList
	 */
	public static void cleanUpResources (List<FileDetails> fileDetailsList, List<JarDetails> jarDetailsList) {
		// delete jars
		for (JarDetails jarDetails : jarDetailsList){
			if (!jarDetails.isJarRequired()) {
				deleteFile (jarDetails.getJarLocation());
			}
		}
		
		//delete files
		for (FileDetails fileDetails : fileDetailsList){
			if (!fileDetails.isFileRequired()) {
				deleteFile (fileDetails.getFileLocations());
			}
		}
	}
	
	private static void deleteFile (String fileName) {
		
		try{
    		File file = new File(fileName);

    		if(file.delete()){
    			LOGGER.info(file.getName() + " is deleted!");
    		}else{
    			LOGGER.info("Delete operation is failed.");
    		}

    	} catch (Exception e){
    		LOGGER.error(e.getMessage(), e);
    	}

	}
	
	public static void main (String args[]) {
		try {

			HttpHost proxy = new HttpHost("172.31.16.242", 8080, "http");
			DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
			CloseableHttpClient httpClient = HttpClients.custom()
			                    .setRoutePlanner(routePlanner)
			                    .build();

			HttpPost getRequest = new HttpPost("");
			getRequest.addHeader("accept", "application/json");

			HttpResponse response = httpClient.execute(getRequest);

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatusLine().getStatusCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(response.getEntity().getContent())));
			
			String output;
			StringBuffer outputBuffer = new StringBuffer("");
			while ((output = br.readLine()) != null) {
				outputBuffer.append(output);
			}
			
			JSONObject jsonObj = new JSONObject(outputBuffer.toString());
			LOGGER.info("MAVEN Compatible Dependency Found: " + jsonObj.getJSONObject ("response").getInt("numFound"));
			
			if (jsonObj.getJSONObject ("response").getInt("numFound") > 1) {
				JSONArray jsonArray = jsonObj.getJSONObject ("response").getJSONArray("docs");
				JSONObject object = (JSONObject) jsonArray.get(0);
				LOGGER.info(object.getString("g"));
				LOGGER.info(object.getString("a"));
				LOGGER.info(object.getString("v"));
			}

		} catch (ClientProtocolException e) {
			LOGGER.error(e.getMessage(), e);

		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
